const express = require('express');
const api = express()
const routes = require('./Routes/Routes')

//CONNECT TO DATABASE
// const UserConnection = require('./Db/UserDb')
// const TaksConnection = require('./Db/TasksDb')

//BODY PARSER
api.use(express.json());

//REDIRECT TO ROUTES
api.use("/", routes);


api.listen('5000', () => {
    console.log('server is runnig on port 5000');
})