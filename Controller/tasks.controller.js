const pool = require('../Db/TasksDb').pool;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const redis = require('redis');
const JWTR = require('jwt-redis').default;
const redisClient = redis.createClient();
const jwtr = new JWTR(redisClient);


module.exports = {
    /**
     * @api {get} /tasks/all Get All Tasks
     * @apiName AllTasks
     * @apiGroup Tasks
     *
     * 
     * @apiSuccess {Number} _id task id
     * @apiSuccess {String} title  task title
     * @apiSuccess {String} description task email
     * @apiSuccess {String} status task status
     * @apiSuccess {Number} ownerid owner id
     *
     * @apiSuccessExample Success
     * {
     * "_id": 7,
     *   "title": "hi",
     *   "description": "hello1",
     *   "status": "TODO",
     *   "ownerid": 1
     * }
     */
    getall: async function(req, res) {
        try {
            const header = req.headers['authorization'];
            const bearer = header.split(' ');
            const token = bearer[1];
            const decoded = await jwtr.verify(token, 'verysecretsecret:)');
            const userId = decoded.id;


            const query = `SELECT * FROM tasks
            WHERE ownerid=${userId};`

            let data = await pool.query(query);
            data = data.rows;

            if (data) return res.status(200).send(data);

        } catch (err) {

            if (err) return res.status(500).send(err.message);

        }
    },
    /**
     * @api {get} /tasks/task/:id Get Tasks BY id
     * @apiName TaskById
     * @apiGroup Tasks
     *
     * 
     * @apiSuccess {Number} _id task id
     * @apiSuccess {String} title  task title
     * @apiSuccess {String} description task email
     * @apiSuccess {String} status task status
     * @apiSuccess {Number} ownerid owner id
     *
     * @apiSuccessExample Success
     * {
     * "_id": 7,
     *   "title": "hi",
     *   "description": "hello1",
     *   "status": "TODO",
     *   "ownerid": 1
     * }
     */
    taskbyid: async function(req, res) {
        const header = req.headers['authorization'];
        const bearer = header.split(' ');
        const token = bearer[1];
        const decoded = await jwtr.verify(token, 'verysecretsecret:)');
        const userId = decoded.id;

        const query = `SELECT * FROM tasks
        WHERE _id = $1 AND ownerid = $2`

        const data = await pool.query(query, [req.params.id, userId])

        if (data.rows.length === 0) return res.status(404).json({ msg: "not found" });

        return res.status(200).send(data.rows);
    },
    /**
     * @api {put} /createtask create new task
     * @apiName CreateTask
     * @apiGroup Tasks
     *
     * @apiParam {String} title task title
     * @apiParam {String} description task description
     * 
     * @apiSuccess {String} _id task id
     * @apiSuccess {String} title  task title
     * @apiSuccess {String} decription task decription
     * @apiSuccess {String} status task status
     * @apiSuccess {Number} ownerid ownerid
     *
     * @apiSuccessExample Success
     * {
     * "_id": 6,
     * "title": "title",
     * "description": "description",
     * "status": "TODO",
     * "ownerid": 1
     * }
     */
    createtask: async function(req, res) {
        try {
            if (!req.body.title && !req.body.description) return res.status(400).json({ msg: "title and description are needed" });
            const header = req.headers['authorization'];
            const bearer = header.split(' ');
            const token = bearer[1];
            const decoded = await jwtr.verify(token, 'verysecretsecret:)');
            const userId = decoded.id;

            const query = ` INSERT INTO tasks(
                title,
                description,
                ownerId,
                status
            )
            VALUES($1,$2,$3,$4);
            `;
            const task = await pool.query(query, [req.body.title, req.body.description, userId, 'TODO']);

            if (task) return res.status(200).json({ msg: "new task created" });

        } catch (err) {

            if (err) return res.status(500).send(err.message);

        }
    },
    /**
     * @api {put} /update/:id/ Update task title or description
     * @apiName UpdateTaskDetail
     * @apiGroup Tasks
     *
     * 
     * @apiSuccess {String} _id task id
     * @apiSuccess {String} title  task title
     * @apiSuccess {String} decription task decription
     * @apiSuccess {String} status task status
     * @apiSuccess {Number} ownerid ownerid
     *
     * @apiSuccessExample Success
     * {
     * "_id": 6,
     * "title": "hi",
     * "description": "done and done",
     * "status": "TODO",
     * "ownerid": 1
     * }
     */
    update: async function(req, res) {
        try {
            const taskId = +req.params.id;

            if (!req.body.title || !req.body.description) return res.status(400).json({ msg: "title or description is empty" });
            const header = req.headers['authorization'];
            const bearer = header.split(' ');
            const token = bearer[1];
            const decoded = await jwtr.verify(token, 'verysecretsecret:)');
            const userId = decoded.id;



            const query = `
                UPDATE tasks
                SET title = $1,
                description = $2
                WHERE _id= $3 AND ownerid = $4
                RETURNING  * ;`;


            let data = await pool.query(query, [req.body.title, req.body.description, taskId, userId]);
            console.log(data.rows);
            if (data.rows.length === 0) return res.status(404).json({ msg: "not found" });


            if (data.rows[0]) return res.status(200).send(data.rows[0]);


        } catch (err) {

            if (err) return res.status(500).send(err.message);

        }
    },

    /**
     * @api {put} /updatestatus/:id/:status Update task status
     * @apiName UpdateTaskStatus
     * @apiGroup Tasks
     *
     * 
     * @apiSuccess {String} _id task id
     * @apiSuccess {String} title  task title
     * @apiSuccess {String} decription task decription
     * @apiSuccess {String} status task status
     * @apiSuccess {Number} ownerid ownerid
     *
     * @apiSuccessExample Success
     * {
     * "_id": 10,
     * "title": "hi",
     * "description": "hello1",
     * "status": "DOING",
     * "ownerid": 2
     * }
     */
    updateStatus: async function(req, res) {
        try {
            const pattern = ['TODO', "DOING", "DONE"];
            let status = req.params.status;
            status = status.toUpperCase();

            const taskId = +req.params.id;

            const header = req.headers['authorization'];
            const bearer = header.split(' ');
            const token = bearer[1];
            const decoded = await jwtr.verify(token, 'verysecretsecret:)');
            const userId = decoded.id;

            const validStatus = pattern.includes(status);

            if (!validStatus) return res.status(400).json({ msg: "bad request" })



            const query = `
                UPDATE tasks
                SET status = $1
                WHERE _id = $2 AND ownerid = $3
                RETURNING  * ;`;

            let data = await pool.query(query, [status, taskId, userId]);

            if (data.rows.length === 0) return res.status(404).json({ msg: "not found" });

            if (data.rows[0]) return res.status(200).send(data.rows[0]);


        } catch (err) {
            if (err) return res.status(500).send(err.message);
        }
    },

    /**
     * @api {delete} /delete/:id Delete task by id
     * @apiName DeleteTaksById
     * @apiGroup Tasks
     *
     * 
     * @apiSuccess {string} msg message
     * @apiSuccess {Object[]} title  task title
     *
     * @apiSuccessExample Success
     * {
     * "msg": "task deleted",
     * "data": [
     *    {
     *        "_id": 1,
     *        "title": "hi",
     *        "description": "done",
     *        "status": "DOING",
     *        "ownerid": 2
     *    }
     *    ]
     * }
     */
    delete: async function(req, res) {
        try {

            const header = req.headers['authorization'];
            const bearer = header.split(' ');
            const token = bearer[1];
            const decoded = await jwtr.verify(token, 'verysecretsecret:)');
            const userId = decoded.id;

            const query = `DELETE FROM tasks
                WHERE _id = ${+req.params.id} AND ownerid = ${userId}
                AND ownerid = ${userId}
                RETURNING  * ;
                `

            let data = await pool.query(query);

            data = data.rows;


            if (data.length === 0) return res.status(400).json({ msg: "not found" });

            console.log(data);
            return res.status(200).json({ msg: "task deleted", data });



        } catch (err) {

            if (err) return res.status(500).send(err.message);

        }
    },

}