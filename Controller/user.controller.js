const pool = require('../Db/UserDb').pool;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const redis = require('redis');
const JWTR = require('jwt-redis').default;
const redisClient = redis.createClient();
const jwtr = new JWTR(redisClient);


module.exports = {
    /**
     * @api {get} /users/all Get All Users
     * @apiName AllUsers
     * @apiGroup Users
     *
     * 
     * @apiSuccess {String} username user's username
     *
     * @apiSuccessExample Success
     * {
     * "username": "alireza",
     * }
     */
    getall: async function(req, res) {
        try {
            const query = `SELECT username FROM users;`

            let data = await pool.query(query);
            data = data.rows;

            if (data) return res.status(200).send(data);

        } catch (err) {

            if (err) return res.status(500).send(err.message);

        }
    },
    /**
     * @api {put} /users/signup signup
     * @apiName UserSignUp
     * @apiGroup Users
     *
     * @apiParam {string} username username
     * @apiParam {string} password password
     *  
     * @apiSuccess {String} msg message
     *
     * @apiSuccessExample Success
     * {
     * "msg": "inserted to table",
     * }
     */
    insetrToTable: async function(req, res) {

        try {
            if (!req.body.username && !req.body.password) return res.status(400).json({ msg: "bad request :/" })

            const password = req.body.password;
            const saltRounds = 10;
            const hashedPassword = await bcrypt.hash(password, saltRounds);


            const query = `INSERT INTO users(
            username,
            password
        )
        VALUES ($1,$2);`;

            const data = await pool.query(query, [req.body.username, hashedPassword]);

            if (data) return res.status(200).json({ msg: "inserted to table" });

        } catch (err) {

            if (err) return res.status(500).send(err.message);

        }
    },
    /**
     * @api {post} /users/signin Sign In
     * @apiName SignIn
     * @apiGroup Users
     *
     * @apiParam {string} username
     * @apiParam {string} password
     * 
     * @apiSuccess {String} token access token
     *
     * @apiSuccessExample Success
     * {
     * "username": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9",
     * }
     */

    signin: async function(req, res) {
        try {
            if (!req.body.username && !req.body.password) return res.status(400).json({ msg: "bad request :/" });

            const query = ` SELECT *
            FROM users
            WHERE username = $1;
            `;
            const user = await pool.query(query, [req.body.username]);

            if (!user.rows[0]) return res.status(404).json({ msg: "user not found" });

            const userPassword = (user.rows[0].password);

            const password = req.body.password;

            const compare = await bcrypt.compare(password, userPassword);

            if (!compare) return res.status(400).json({ msg: "user not found" });

            if (compare) {
                const token = await jwtr.sign({ username: user.rows[0].username, id: user.rows[0]._id }, 'verysecretsecret:)', { expiresIn: 60000 * 10 });
                console.log(token);
                return res.status(200).json({ token: token });
            }


        } catch (err) {

            if (err) return res.status(500).send(err.message);

        }
    },
    /**
     * @api {put} /users/update change password
     * @apiName ChangeUserName
     * @apiGroup Users
     *
     * @apiParam {string} password
     * 
     * @apiSuccess {String} msg password chanegd you need to login again
     *
     * @apiSuccessExample Success
     * {
     *     "msg": "password chanegd you need to login again"
     * }
     */
    update: async function(req, res) {
        try {

            //GET USER ID FROM TOKEN
            const header = req.headers['authorization'];
            const bearer = header.split(' ');
            const token = bearer[1];
            const decoded = await jwtr.verify(token, 'verysecretsecret:)');
            const userId = decoded.id;



            if (!req.body.password) return res.status(400).json({ msg: "bad request :/" });


            const password = req.body.password;
            const saltRounds = 10;
            const hashedPassword = await bcrypt.hash(password, saltRounds);


            const query = `
            UPDATE users
            SET password = $2
            WHERE _id= $1
            RETURNING  * ;`;

            let data = await pool.query(query, [userId, hashedPassword]);

            await jwtr.destroy(decoded.jti)

            if (data.rows) return res.status(200).json({ msg: "password chanegd you need to login again" });

            if (!data.rows.length) return res.status(404).json({ msg: "user not found" });

        } catch (err) {

            if (err) return res.status(500).send(err.message);

        }
    },


    /**
     * @api {get} /users/logout logout
     * @apiName LogOut
     * @apiGroup Users
     *
     * 
     * @apiSuccess {String} msg loged out
     *
     * @apiSuccessExample Success
     * {
     *     "msg": "loged out"
     * }
     * 
     */
    logout: async function(req, res) {
        const header = req.headers['authorization'];

        try {
            const bearer = header.split(' ');
            const token = bearer[1];
            var decoded = await jwtr.verify(token, 'verysecretsecret:)');
            await jwtr.destroy(decoded.jti)
            res.status(200).json({ msg: "loged out" })
        } catch (err) {
            if (err) return res.status(500).send(err.message);

        }


    },
}