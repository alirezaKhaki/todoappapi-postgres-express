const tools = {};

//NOT FOUND RESPONSE
tools.notFound = function(req, res, next) {
    if (!req.session.user) {
        return res.redirect("/api/login")
    };

    return next()
};

module.exports = tools;