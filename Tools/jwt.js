const jwt = require('jsonwebtoken');
const auth = {};
var redis = require('redis');
var JWTR = require('jwt-redis').default;
var redisClient = redis.createClient();
var jwtr = new JWTR(redisClient);

auth.validate = async function(req, res, next) {
    //get header
    const header = req.headers['authorization'];
    if (header) {
        try {
            const bearer = header.split(' ');
            const token = bearer[1];

            var decoded = await jwtr.verify(token, 'verysecretsecret:)');
            // console.log(decoded)
            req.token = token;
            next();
        } catch (err) {
            if (err) return res.status(403).json({ msg: "access denied" });
        }


    } else {
        return res.status(403).json({ msg: "access denied" });
    }
};


module.exports = auth;