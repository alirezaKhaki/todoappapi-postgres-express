const router = require('express').Router();
const tasksController = require('../Controller/tasks.controller');
const auth = require('../Tools/jwt');


//NEW TASK
router.post('/createtask', auth.validate, tasksController.createtask)

//GET ALL
router.get('/all', auth.validate, tasksController.getall)

//GET TASK BY ID
router.get('/task/:id', auth.validate, tasksController.taskbyid)

//UPDATE
router.put('/update/:id', auth.validate, tasksController.update)

//UPDATE STATUS
router.put('/updatestatus/:id/:status', auth.validate, tasksController.updateStatus)

//DELETE
router.delete('/delete/:id', auth.validate, tasksController.delete)



module.exports = router;