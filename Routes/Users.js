const router = require('express').Router();
const userController = require('../Controller/user.controller');
const auth = require('../Tools/jwt');


//SIGN IN
router.post('/signin', userController.signin)

//GET ALL
router.get('/all', auth.validate, userController.getall)


//SIGNUP
router.post('/signup', userController.insetrToTable)


//UPDATE
router.put('/update/', auth.validate, userController.update)


//LOGOUT
router.get('/logout', auth.validate, userController.logout)




module.exports = router;