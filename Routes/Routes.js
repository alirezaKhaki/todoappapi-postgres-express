const router = require('express').Router();
const users = require('./Users')
const tasks = require('./Tasks')

//REDIRECT TO USERS ROUTE
router.use('/users', users);
//REDIRECT TO TASKS ROUTE
router.use('/tasks', tasks);



module.exports = router;