define({ "api": [
  {
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "optional": false,
            "field": "varname1",
            "description": "<p>No type.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "varname2",
            "description": "<p>With type.</p>"
          }
        ]
      }
    },
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "./docs/main.js",
    "group": "C:\\Users\\Arshin-Dev02\\Desktop\\test\\New folder\\todoappapi-postgres-express\\docs\\main.js",
    "groupTitle": "C:\\Users\\Arshin-Dev02\\Desktop\\test\\New folder\\todoappapi-postgres-express\\docs\\main.js",
    "name": ""
  },
  {
    "type": "get",
    "url": "/tasks/all",
    "title": "Get All Tasks",
    "name": "AllTasks",
    "group": "Tasks",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "_id",
            "description": "<p>task id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>task title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>task email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>task status</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ownerid",
            "description": "<p>owner id</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "{\n\"_id\": 7,\n  \"title\": \"hi\",\n  \"description\": \"hello1\",\n  \"status\": \"TODO\",\n  \"ownerid\": 1\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./Controller/tasks.controller.js",
    "groupTitle": "Tasks"
  },
  {
    "type": "put",
    "url": "/createtask",
    "title": "create new task",
    "name": "CreateTask",
    "group": "Tasks",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>task title</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>task description</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>task id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>task title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "decription",
            "description": "<p>task decription</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>task status</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ownerid",
            "description": "<p>ownerid</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "{\n\"_id\": 6,\n\"title\": \"title\",\n\"description\": \"description\",\n\"status\": \"TODO\",\n\"ownerid\": 1\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./Controller/tasks.controller.js",
    "groupTitle": "Tasks"
  },
  {
    "type": "delete",
    "url": "/delete/:id",
    "title": "Delete task by id",
    "name": "DeleteTaksById",
    "group": "Tasks",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "msg",
            "description": "<p>message</p>"
          },
          {
            "group": "Success 200",
            "type": "Object[]",
            "optional": false,
            "field": "title",
            "description": "<p>task title</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "{\n\"msg\": \"task deleted\",\n\"data\": [\n   {\n       \"_id\": 1,\n       \"title\": \"hi\",\n       \"description\": \"done\",\n       \"status\": \"DOING\",\n       \"ownerid\": 2\n   }\n   ]\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./Controller/tasks.controller.js",
    "groupTitle": "Tasks"
  },
  {
    "type": "get",
    "url": "/tasks/task/:id",
    "title": "Get Tasks BY id",
    "name": "TaskById",
    "group": "Tasks",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "_id",
            "description": "<p>task id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>task title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "description",
            "description": "<p>task email</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>task status</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ownerid",
            "description": "<p>owner id</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "{\n\"_id\": 7,\n  \"title\": \"hi\",\n  \"description\": \"hello1\",\n  \"status\": \"TODO\",\n  \"ownerid\": 1\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./Controller/tasks.controller.js",
    "groupTitle": "Tasks"
  },
  {
    "type": "put",
    "url": "/update/:id/",
    "title": "Update task title or description",
    "name": "UpdateTaskDetail",
    "group": "Tasks",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>task id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>task title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "decription",
            "description": "<p>task decription</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>task status</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ownerid",
            "description": "<p>ownerid</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "{\n\"_id\": 6,\n\"title\": \"hi\",\n\"description\": \"done and done\",\n\"status\": \"TODO\",\n\"ownerid\": 1\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./Controller/tasks.controller.js",
    "groupTitle": "Tasks"
  },
  {
    "type": "put",
    "url": "/updatestatus/:id/:status",
    "title": "Update task status",
    "name": "UpdateTaskStatus",
    "group": "Tasks",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "_id",
            "description": "<p>task id</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "title",
            "description": "<p>task title</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "decription",
            "description": "<p>task decription</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>task status</p>"
          },
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "ownerid",
            "description": "<p>ownerid</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "{\n\"_id\": 10,\n\"title\": \"hi\",\n\"description\": \"hello1\",\n\"status\": \"DOING\",\n\"ownerid\": 2\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./Controller/tasks.controller.js",
    "groupTitle": "Tasks"
  },
  {
    "type": "get",
    "url": "/users/all",
    "title": "Get All Users",
    "name": "AllUsers",
    "group": "Users",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>user's username</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "{\n\"username\": \"alireza\",\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./Controller/user.controller.js",
    "groupTitle": "Users"
  },
  {
    "type": "put",
    "url": "/users/update",
    "title": "change password",
    "name": "ChangeUserName",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>password chanegd you need to login again</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"msg\": \"password chanegd you need to login again\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./Controller/user.controller.js",
    "groupTitle": "Users"
  },
  {
    "type": "get",
    "url": "/users/logout",
    "title": "logout",
    "name": "LogOut",
    "group": "Users",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>loged out</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "{\n    \"msg\": \"loged out\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./Controller/user.controller.js",
    "groupTitle": "Users"
  },
  {
    "type": "post",
    "url": "/users/signin",
    "title": "Sign In",
    "name": "SignIn",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "username",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>access token</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "{\n\"username\": \"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9\",\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./Controller/user.controller.js",
    "groupTitle": "Users"
  },
  {
    "type": "put",
    "url": "/users/signup",
    "title": "signup",
    "name": "UserSignUp",
    "group": "Users",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "username",
            "description": "<p>username</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>password</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "msg",
            "description": "<p>message</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "{\n\"msg\": \"inserted to table\",\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./Controller/user.controller.js",
    "groupTitle": "Users"
  }
] });
