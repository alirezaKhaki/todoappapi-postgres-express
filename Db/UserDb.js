const { Pool } = require('pg')

const connectionString = 'postgresql://postgres:ALI_reza@localhost:5432/test';


const pool = new Pool({
    connectionString: connectionString,
});

module.exports = {
    connect: (function() {

        pool.on('connect', () => {
            console.log('connected to the db');
        });
        console.log("db/make: Connecting to database...");
    })(),

    createUsersTable: (async() => {
        try {
            const query = `CREATE TABLE IF NOT EXISTS users(
            _id SERIAL PRIMARY KEY,
            username VARCHAR(45) UNIQUE,
            password VARCHAR(255) NOT NULL
        );`;
            const table = await pool.query(query);
            if (table) return console.log('users table created');
        } catch (error) {
            console.log(error)
        }
    })(),
    pool: pool
}