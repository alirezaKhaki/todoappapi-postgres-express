const { Pool } = require('pg')

const connectionString = 'postgresql://postgres:ALI_reza@localhost:5432/test';


const pool = new Pool({
    connectionString: connectionString,
});


module.exports = {
    connect: (function() {

        pool.on('connect', () => {
            console.log('connected to the db');
        });
        console.log("db/make: Connecting to database...");
    })(),
    createTasksTable: (async() => {
        try {
            const query = `CREATE TABLE IF NOT EXISTS tasks(
            _id SERIAL PRIMARY KEY,
            title VARCHAR(255) NOT NULL,
            description VARCHAR(255) NOT NULL,
            status varchar(50) NOT NULL,
            ownerId INTEGER NOT NULL,
                FOREIGN KEY(ownerId) 
                    REFERENCES users(_id)
        );`;
            const table = await pool.query(query);
            if (table) return console.log('tasks table created');
        } catch (error) {
            console.log(error)
        }
    })(),
    pool: pool
}